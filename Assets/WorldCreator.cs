﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldCreator : MonoBehaviour {

  public Dooda rockPrefab;
  public Dooda treePrefab;
  public Dooda grassPrefab;

  public int size = 32;

  public GameObject group;

  void Awake() {
    group = gameObject.EmptyGroup("doodas");
    Game.I.map = CreateRandom(size, RNG.Instance);
  }

  public Map CreateRandom(int size, RNG rng) {
    // create map
    Map map = new Map(size, size);
    for (int row = 0, i = 0; row < map.Rows; row++) {
      for (int col = 0; col < map.Cols; col++, i++) {
        Tile tile = map.At(i);
        tile.faction = rng.RandomInt(3);
        //tile.AddGold(Mathf.Max(0, rng.RandomInt(30) - 20).Square());
      }
    }
    // place rocks/trees/grass
    PlaceDoodas(map, rockPrefab, 0.13f, 0.13f, rng);
    PlaceDoodas(map, treePrefab, 0.34f, 0.07f, rng);
    PlaceDoodas(map, grassPrefab, 0.27f, 0.05f, rng);
    return map;
  }

  void PlaceDoodas(Map map, Dooda prefab, float coverage, float frequency, RNG rng) {
    Tile[] tiles = map.Tiles.Shuffle(rng);
    int num_to_place = Mathf.RoundToInt(tiles.Length * coverage * rng.RandomFloat(0.80f, 1.20f));
    int iterations = 10;
    float offset = 0.8f;
    while (num_to_place > 0 && iterations > 0) {
      PerlinNoise3D perlin = new PerlinNoise3D();
      perlin.Octaves = 2;
      perlin.Frequency = frequency;
      perlin.Amplitude = 1.0f;
      foreach (var tile in tiles) {
        if (perlin.Compute(tile.cell.x, tile.cell.y, 0f) > offset) {
          if (PlaceDooda(tile, prefab, rng)) {
            num_to_place--;
          }
        }
      }
      offset *= 0.9f;
      iterations--;
    }
  }

  bool PlaceDooda(Tile tile, Dooda prefab, RNG rng) {
    if (tile.dooda != null) {
      return false;
    }
    Dooda dooda = Instantiate(prefab);
    dooda.transform.parent = group.transform;
    Vector3 offset = rng.RandomVector3MP(0.25f);
    offset.z *= 0.35f;
    dooda.transform.position = MapBakery.CellCenter3(tile.cell) + offset;
    dooda.transform.rotation = rng.RandomYRotationDeg(360f) * rng.RandomRotationDeg(5f);
    tile.dooda = dooda;
    dooda.tile = tile;
    return true;
  }

}
