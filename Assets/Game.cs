﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Game : MonoBehaviour {

  public static Game I;

  public float goldStart = 2500f;
  public float goldRateBase = 1f;
  public float goldRateMang = 0.5f;
  public int goldAmountBase = 1;
  public int goldAmountWhite = 2;
  public int numNeighbours = 3;

  public int maxGoldPerTile = 1000;

  public Color[] workColors;

  public Map map;
  public Map Map {  get { return map; } }

  public BlobManager blobManager;

  public WorldCreator creator;

  void Awake() {
    I = this;
  }

  void Start() {
    AddGold(new Vector2Int(Map.Rows/2, Map.Cols/2), Map.Rows/4, goldStart);
  }

  public float TotalGold {
    get {
      return Map.Tiles.Sum(tile => tile.Gold);
    }
  }

  // Returns how much gold was added
  public float AddGold(float gold) {
    float start_gold = gold;
    Tile[] tiles = map.Tiles.Shuffle(RNG.Instance);
    for (int i=0; i<tiles.Length && gold > 0f; i++) {
      gold -= tiles[i].AddGold(gold);
    }
    return start_gold - gold;
  }
  public float AddGold(Vector2Int cell, float radius, float gold) {
    float start_gold = gold;
    Tile[] tiles = map.Tiles.Where(tile => (tile.cell - cell).magnitude <= radius).Shuffle(RNG.Instance);
    for (int i = 0; i < tiles.Length && gold > 0f; i++) {
      gold -= tiles[i].AddGold(gold);
    }
    return start_gold - gold;
  }
}
