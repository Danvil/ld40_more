﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public enum Role {
  NONE = 0, GRUNT = 1, BLUE = 2, WHITE = 3, MANG = 4, BOSS = 5, _count = 6
}

public class Blob : MonoBehaviour {

  public Color workerColor;
  public Color idleColor;

  AudioSource audioSource;

  void Awake() {
    audioSource = GetComponent<AudioSource>();
    IsIdle = true;
  }

  private void OnDrawGizmos() {
    Gizmos.color = Color.green;
    if (!IsTargetReached) {
      Gizmos.DrawLine(Position.ToVector3(), MapBakery.CellCenter3(TargetCell));
    }
  }

  public void Update() {
    ExecutePlan();
    UpdateMove(Game.I.map, Time.deltaTime);
  }

  #region move

  public static Vector2Int InvalidCell = new Vector2Int(-1, -1);

  Vector2 position;
  public Vector2 Position { get { return position; } }
  Vector2Int currentCell = InvalidCell;
  public Vector2Int CurrentCell { get { return currentCell; } }
  Vector2Int targetCell = InvalidCell;
  public Vector2Int TargetCell {
    get {
      return targetCell;
    }
    set {
      if (targetCell != value) {
        targetCell = value;
        IsTargetReached = false;
        hasNextCell = false;
      }
    }
  }
  public bool IsTargetReached { get; private set; }
  bool hasNextCell;
  Vector2Int nextCell = InvalidCell;

  public void Reposition(Map map, Vector2 position) {
    this.position = position;
    UpdateFrontend();
    UpdateCurrentTile(map, MapBakery.PositionToCell(position));
    this.targetCell = currentCell;
    IsTargetReached = true;
    hasNextCell = false;
  }

  public Renderer blobRenderer;

  bool _isIdle = true;
  public bool IsIdle {
    get { return _isIdle; }
    set {
      _isIdle = value;
      blobRenderer.material.color = _isIdle ? idleColor : workerColor;
    }
  }
  public float idleSpeed = 0.75f;
  public float nonIdleSpeed = 1.5f;

  public float Speed {
    get {
      Tile tile = Game.I.map.At(currentCell);
      float tile_speed = tile == null ? 1f : tile.Speed;
      return (IsIdle ? idleSpeed : nonIdleSpeed) * tile_speed;
    }
  }

  void UpdateMove(Map map, float dt) {
    if (IsTargetReached) {
      return;
    }
    if (!hasNextCell) {
      // stupid walk on grid
      Vector2Int delta = targetCell - currentCell;
      if (delta.x != 0 || delta.y != 0) {
        if (Mathf.Abs(delta.x) < Math.Abs(delta.y)) {
          nextCell = currentCell + new Vector2Int(0, delta.y < 0 ? -1 : +1);
        } else {
          nextCell = currentCell + new Vector2Int(delta.x < 0 ? -1 : +1, 0);
        }
        hasNextCell = true;
      } else {
        IsTargetReached = true;
      }
    }
    if (hasNextCell) {
      // check if we need to move
      Vector2 target = MapBakery.CellCenter2(nextCell);
      Vector2 delta = target - position;
      float delta_len = delta.magnitude;
      float step = Speed * dt;
      if (delta_len <= step) {
        hasNextCell = false;
        //position = target;
      } else {
        position += (step / delta_len) * delta;
        Vector2Int newCurrentTile = MapBakery.PositionToCell(position);
        UpdateCurrentTile(map, newCurrentTile);
        UpdateFrontend();
      }
    }
  }

  void UpdateFrontend() {
    transform.position = new Vector3(position.x, 0f, position.y);
  }

  void UpdateCurrentTile(Map map, Vector2Int newCell) {
    if (newCell == currentCell) {
      return;
    }
    map.RemoveBlob(this);
    currentCell = newCell;
    map.AddBlob(this);
  }

  #endregion

  #region hauling

  public float maxHauledGold = 500f;

  public float pickupScale = 0.35f;

  public Dooda goldDoodaPrefab;

  public Dooda haul;

  public void Drop() {
    if (haul == null) {
      return;
    }
    haul.transform.parent = Game.I.creator.group.transform;
    haul.transform.position = MapBakery.CellCenter3(CurrentCell);
    haul.Drop(0.65f);
    haul = null;
  }

  public float WithdrawGold(float amount) {
    if (haul == null || haul.resource != Resource.GOLD) {
      return 0f;
    }
    amount = Mathf.Min(amount, haul.amount);
    haul.amount -= amount;
    if (haul.amount <= 0f) {
      Destroy(haul.gameObject);
      haul = null;
    }
    return amount;
  }

  public void PickupGold(float amount) {
    if (haul != null && haul.resource != Resource.GOLD) {
      Drop();
    }
    if (haul == null) {
      haul = GameObject.Instantiate(goldDoodaPrefab);
      haul.transform.parent = transform;
      haul.transform.localPosition = new Vector3(0f, 0.5f, 0f);
      haul.transform.localScale = new Vector3(pickupScale, pickupScale, pickupScale);
    }
    amount = Mathf.Min(maxHauledGold, amount);
    if (amount < haul.amount) {
      return;
    }
    amount -= haul.amount;
    Game.I.Map.IterateTile(CurrentCell - new Vector2Int(1, 1), CurrentCell + new Vector2Int(2, 2),
      tile => {
        float dgold = tile.RemoveGold(amount);
        amount -= dgold;
        haul.amount += dgold;
      });
  }

  public void Pickup(Dooda dooda) {
    if (dooda == null) {
      return;
    }
    if (dooda.resource == Resource.GOLD) {
      PickupGold(dooda.amount);
      return;
    }
    if (haul != null) {
      Drop();
    }
    dooda.tile.dooda = null;
    dooda.tile = null;
    haul = dooda;
    dooda.transform.parent = transform;
    dooda.transform.localPosition = new Vector3(0f, 0.5f, 0f);
    dooda.transform.localScale = new Vector3(pickupScale, pickupScale, pickupScale);
    audioSource.PlayOneShot(audioSource.clip);
  }

  #endregion

  #region planning

  public class Task {
    public enum Result { FAIL, SUCCESS, RUN };
    Func<bool> start;
    Func<Result> step;
    Action stop;
    bool isStarted;
    Result state;

    public Task(Func<bool> start, Func<Result> step, Action stop) {
      this.start = start;
      this.step = step;
      this.stop = stop;
      isStarted = false;
      state = Result.RUN;
    }

    public Result Tick() {
      if (!isStarted) {
        state = start() ? Result.RUN : Result.FAIL;
        isStarted = true;
      }
      if (state != Result.RUN) {
        return state;
      }
      state = step();
      if (state != Result.RUN) {
        stop();
      }
      return state;
    }
  }

  List<Task> plan = null;

  public bool HasPlan { get { return plan != null && plan.Count > 0; } }

  public void SetPlan(IEnumerable<Task> plan) {
    this.plan = plan.ToList();
  }

  public void ExecutePlan() {
    if (plan == null || plan.Count == 0) {
      return;
    }
    var state = plan.First().Tick();
    if (state == Task.Result.FAIL) {
      plan = null;
    }
    if (state == Task.Result.SUCCESS) {
      plan.RemoveAt(0);
    }
  }

  #endregion

}
