﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class MapBakery {
  public static Vector3 CellAnchor3(Vector2Int p) {
    return new Vector3(p.x, 0f, p.y);
  }
  public static Vector3 CellCenter3(Vector2Int p) {
    return CellCenter3(p.x, p.y);
  }
  public static Vector3 CellCenter3(int row, int col) {
    return new Vector3(row + 0.5f, 0f, col + 0.5f);
  }
  public static Vector2 CellCenter2(Vector2Int p) {
    return CellCenter2(p.x, p.y);
  }
  public static Vector2 CellCenter2(int row, int col) {
    return new Vector2(row + 0.5f, col + 0.5f);
  }
  public static Vector2Int PositionToCell(Vector2 p) {
    return new Vector2Int{ x = (int)p.x, y = (int)p.y };
  }
}

public class Map {
  public int Rows { get { return rows; } }
  public int Cols { get { return cols; } }
  public Map(int rows, int cols) {
    this.rows = rows;
    this.cols = cols;
    this.tiles = new Tile[rows * cols];
    for (int row=0, i=0; row<rows; row++) {
      for (int col=0; col<cols; col++, i++) {
        this.tiles[i] = new Tile(new Vector2Int(row, col));
      }
    }
  }
  public Tile At(int index) {
    return tiles[index];
  }
  public bool InBounds(int row, int col) {
    return 0 <= row && row < rows && 0 <= col && col < cols;
  }
  public int Index(Vector2Int cell) {
    return Index(cell.x, cell.y);
  }
  public int Index(int row, int col) {
    return row * cols + col;
  }
  public Tile At(Vector2Int cell) {
    return At(cell.x, cell.y);
  }
  public Tile At(int row, int col) {
    if (!InBounds(row, col)) {
      return null;
    } else {
      return tiles[Index(row, col)];
    }
  }
  private Tile[] tiles;
  public IEnumerable<Tile> Tiles { get { return tiles; } }
  private int rows, cols;

  public void IterateTile(Vector2Int lower, Vector2Int upper, Action<Tile> action) {
    IterateTile(lower.x, lower.y, upper.x, upper.y, action);
  }
  public void IterateTile(int row0, int col0, int row1, int col1, Action<Tile> action) {
    row0 = Mathf.Max(row0, 0);
    row1 = Mathf.Min(row1, rows);
    col0 = Mathf.Max(col0, 0);
    col1 = Mathf.Min(col1, cols);
    for (int row = row0; row < row1; row++) {
      for (int col = col0; col < col1; col++) {
        action(At(row, col));
      }
    }
  }

  public Tile RandomTile(RNG rng) {
    return tiles[rng.RandomInt(tiles.Length)];
  }

  public void RemoveBlob(Blob blob) {
    Tile currentTile = At(blob.CurrentCell);
    if (currentTile == null) {
      return;
    }
    currentTile.RemoveBlob(blob);
  }

  public void AddBlob(Blob blob) {
    Tile currentTile = At(blob.CurrentCell);
    if (currentTile == null) {
      return;
    }
    currentTile.AddBlob(blob);
  }
}

