﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Scheduler : MonoBehaviour {

  public BlobManager blobs;
  public WorkpenManager pens;

  List<Blob> ideling = new List<Blob>();

	void Start() { }
	
	void Update() {
    // Ideling blobs
		foreach (Blob blob in blobs.Blobs) {
      if (!blob.HasPlan) {
        MoveToRandomGoal(blob);
        ideling.Add(blob);
        blob.IsIdle = true;
      }
    }
    // workpens
    foreach (Workpen pen in pens.Workpens) {
      // un-assign idle workers from pens
      if (ideling.Contains(pen.worker)) {
        pen.worker = null;
      }
      // if no worker try to find one
      if (pen.worker != null) {
        continue;
      }
      Blob worker = FindClosestIdelBlob(pen.cell);
      if (worker == null) {
        continue;
      }
      // assign worker
      pen.worker = worker;
      worker.IsIdle = false;
      ideling.Remove(worker);
      if (pen.IsOperational) {
        if (pen.dooda != null) {
          OperateWorkpen(worker, pen);
        } else {
          if (pen.consumesGold) {
            FetchGold(worker, pen);
          } else {
            FetchDooda(worker, pen);
          }
        }
      } else {
        Construct(worker, pen);
      }
    }
	}

  Blob FindClosestIdelBlob(Vector2Int cell) {
    return ideling.OrderBy(blob => (blob.CurrentCell - cell).sqrMagnitude).FirstOrDefault();
  }

  Blob.Task MoveToTask(Blob blob, Func<Vector2Int?> cellf) {
    return new Blob.Task(
      () => { Vector2Int? cell = cellf(); if (cellf == null) return false; blob.TargetCell = cell.Value; return true; },
      () => blob.IsTargetReached ? Blob.Task.Result.SUCCESS : Blob.Task.Result.RUN,
      () => { blob.TargetCell = blob.CurrentCell; });
  }

  Blob.Task MoveToGoldTask(Blob blob) {
    return MoveToTask(blob, () => {
      // find closest tile with gold
      Tile closest_gold_tile = Game.I.Map.Tiles
        .Where(tile => tile.Gold > 0f)
        .OrderBy(tile => (tile.cell - blob.CurrentCell).sqrMagnitude)
        .FirstOrDefault();
      if (closest_gold_tile == null) {
        return null;
      }
      return closest_gold_tile.cell;
    });
  }

  Blob.Task PickUpGoldTask(Blob blob, Func<float> amountf) {
    return new Blob.Task(
      () => true,
      () => {
        Tile tile = Game.I.Map.At(blob.CurrentCell);
        blob.PickupGold(amountf());
        return Blob.Task.Result.SUCCESS;
      },
      () => { });
  }

  Blob.Task PickUpDoodaTask(Blob blob) {
    return new Blob.Task(
      () => true,
      () => {
        Tile tile = Game.I.Map.At(blob.CurrentCell);
        if (tile.dooda == null) {
          return Blob.Task.Result.FAIL;
        }
        blob.Pickup(tile.dooda);
        return Blob.Task.Result.SUCCESS;
      },
      () => { });
  }

  //Blob.Task PutDownGoldTask(Blob blob, Vector2Int cell) {
  //  return new Blob.Task(
  //    () => true,
  //    () => {
  //      if (blob.haul == null || blob.haul)
  //      Game.I.Map.IterateTile(blob.CurrentCell - new Vector2Int(1, 1), blob.CurrentCell + new Vector2Int(2, 2),
  //        tile => {
  //          blob.hauledGold -= tile.AddGold(blob.hauledGold);
  //        });
  //      return Blob.Task.Result.SUCCESS;
  //    },
  //    () => { });
  //}

  Blob.Task ConstructTask(Blob blob, Workpen workpen) {
    return new Blob.Task(
      () => {
        return (blob.CurrentCell - workpen.cell).magnitude < 2;
      },
      () => {
        if (blob.haul == null || blob.haul.resource != Resource.GOLD) {
          return Blob.Task.Result.FAIL;
        }
        float dgold = blob.WithdrawGold(workpen.StillToPayForConstruction);
        workpen.AddGoldForConstruction(dgold);
        return Blob.Task.Result.SUCCESS;
      },
      () => { });
  }

  Blob.Task FeedWorkpenTask(Blob blob, Workpen workpen) {
    return new Blob.Task(
      () => blob.CurrentCell == workpen.cell,
      () => {
        workpen.dooda = blob.haul;
        blob.Drop();
        return Blob.Task.Result.SUCCESS;
      },
      () => { });
  }

  Blob.Task WaitTask(Blob blob, Func<Blob.Task.Result> checkf) {
    return new Blob.Task(
      () => true,
      () => checkf(),
      () => { });
  }

  void MoveToRandomGoal(Blob blob) {
    blob.SetPlan(new Blob.Task[] {
      MoveToTask(blob, () => RNG.Instance.RandomVector2Int(0, Game.I.Map.Rows, 0, Game.I.Map.Cols))
    });
  }

  void Construct(Blob blob, Workpen workpen) {
    blob.SetPlan(new Blob.Task[] {
      MoveToGoldTask(blob),
      PickUpGoldTask(blob, () => workpen.StillToPayForConstruction),
      MoveToTask(blob, () => workpen.cell),
      ConstructTask(blob, workpen)
    });
  }

  void FetchDooda(Blob blob, Workpen workpen) {
    blob.SetPlan(new Blob.Task[] {
      MoveToTask(blob, () => {
        // find closest tile with a dooda
        Tile closest_dooda_tile = Game.I.Map.Tiles
            .Where(tile => tile.dooda != null && workpen.IsValidInput(tile.dooda))
            .OrderBy(tile => (tile.cell - blob.CurrentCell).sqrMagnitude)
            .FirstOrDefault();
        if (closest_dooda_tile == null) {
          return null;
        }
        return closest_dooda_tile.cell;
      }),
      PickUpDoodaTask(blob),
      MoveToTask(blob, () => workpen.cell),
      FeedWorkpenTask(blob, workpen)
    });
  }

  void FetchGold(Blob blob, Workpen workpen) {
    blob.SetPlan(new Blob.Task[] {
      MoveToGoldTask(blob),
      PickUpGoldTask(blob, () => workpen.StillToPayForConstruction),
      MoveToTask(blob, () => workpen.cell),
      FeedWorkpenTask(blob, workpen)
    });
  }

  void OperateWorkpen(Blob blob, Workpen workpen) {
    blob.SetPlan(new Blob.Task[] {
      MoveToTask(blob, () => workpen.cell),
      WaitTask(blob, () => {
        Tile tile = Game.I.Map.At(blob.CurrentCell);
        if (tile == null || tile.workpen != workpen || workpen == null) return Blob.Task.Result.FAIL;
        if (workpen.dooda == null) return Blob.Task.Result.FAIL;
        if (tile.workpen.worker != blob) return Blob.Task.Result.SUCCESS;
        return Blob.Task.Result.RUN;
      })
    });
  }
}
