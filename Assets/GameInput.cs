﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameInput : MonoBehaviour {

  public WorkpenManager workpenManager;

  public float mouseSensitivityX = -0.04f;
  public float mouseSensitivityY = -0.08f;

  Vector3 lastPosition;

  Workpen.Type workpenType = Workpen.Type.GOLD;

	void Update() {
    if (Input.GetKeyDown(KeyCode.G)) {
      workpenType = Workpen.Type.GOLD;
    }
    if (Input.GetKeyDown(KeyCode.C)) {
      workpenType = Workpen.Type.CLONE;
    }
    Vector2 mp = MouseFloorPosition();
    if (Input.GetMouseButtonUp(0)) {
      workpenManager.CreateWorkpen(mp, workpenType);
    }
    if (Input.GetMouseButtonDown(1)) {
      lastPosition = Input.mousePosition;
    }
    if (Input.GetMouseButton(1)) {
      //workpenManager.RemoveWorkpen(mp);
      Vector3 delta = Input.mousePosition - lastPosition;
      float h = Camera.main.transform.position.y;
      Camera.main.transform.Translate(delta.x * mouseSensitivityX, delta.y * mouseSensitivityY, 0f);
      Camera.main.transform.position = new Vector3(Camera.main.transform.position.x, h, Camera.main.transform.position.z);
      lastPosition = Input.mousePosition;
    }
  }

  public static Vector2 MouseFloorPosition() {
    Ray ray = Camera.main.ScreenPointToRay(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0f));
    // solve (a + l*u).y == 0
    float lambda = -ray.origin.y / ray.direction.y;
    Vector3 floorPos = ray.origin + lambda * ray.direction;
    return new Vector2(floorPos.x, floorPos.z);
  }
}
