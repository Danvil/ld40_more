﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Resource {
  GOLD, TREE, ROCK, GRASS
}

public class Dooda : MonoBehaviour {
  public Resource resource = Resource.GOLD;

  public float amount = 100f;
  public float conversionRate = 1f;

  bool isGrounded = true;
  public float hoverSpeed = 1f;
  public float hoverAmount = 0f;

  public Tile tile;

  float hoverTime = 0f;

  float localZOffset = 0f;

  public float reproductionRate = 0.0f;

  public void Drop(float z) {
    isGrounded = false;
    localZOffset = z;
  }

  void Update() {
    if (!isGrounded) {
      hoverTime += Time.deltaTime;
      float hoverX = Mathf.Sin(hoverTime * Mathf.PI * 2.0f * hoverSpeed) * hoverAmount;
      this.transform.localPosition = new Vector3(this.transform.localPosition.x, localZOffset + hoverX, this.transform.localPosition.z);
    }
    if (isGrounded && tile != null && reproductionRate > 0f) {
      if (RNG.Instance.RandomEvent(reproductionRate, Time.deltaTime)) {
        // find neighbour
        Tile neighbour = null;
        Game.I.Map.IterateTile(tile.cell - new Vector2Int(1, 1), tile.cell + new Vector2Int(2, 2),
            n => { if (n != tile && !n.IsBlocked) neighbour = n; });
        if (neighbour != null) {
          Dooda clone = Instantiate(this);
          clone.transform.parent = transform.parent;
          clone.transform.position = MapBakery.CellCenter3(neighbour.cell);
          neighbour.dooda = clone;
          clone.tile = neighbour;
        }
      }
    }
  }
}
