﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter))]
public class MapGold : MonoBehaviour {

  Mesh mesh;

  void Start () {
    mesh = new Mesh();
    GetComponent<MeshFilter>().mesh = mesh;
    InvokeRepeating("UpdateGold", 0f, 0.25f);
  }

  void UpdateGold() {
    Bake(Game.I.Map, mesh);
  }

  public float tileScale = 0.65f;

  public void Bake(Map map, Mesh mesh) {
    MeshData md = new MeshData();
    float s = 0.5f * tileScale;
    for (int row = 0; row < map.Rows; row++) {
      for (int col = 0; col < map.Cols; col++) {
        Tile tile = map.At(row, col);
        if (tile.Gold == 0) {
          continue;
        }
        float h = 0.05f + 1f * (float)tile.Gold / Game.I.maxGoldPerTile;
        Vector3 center = MapBakery.CellCenter3(row, col);
        Vector3[] corners = new Vector3[] {
            center + new Vector3(-s, 0, -s),
            center + new Vector3(-s, 0, +s),
            center + new Vector3(+s, 0, +s),
            center + new Vector3(+s, 0, -s)
          };
        center += new Vector3(0, h, 0);
        for (int i = 0; i < 4; i++) {
          Vector3 p0 = center;
          Vector3 p1 = corners[i];
          Vector3 p2 = corners[i == 3 ? 0 : i + 1];
          Vector3 normal = Vector3.Cross(p1 - p0, p2 - p0);
          md.AddTriangle(new Vector3[] { p0, p1, p2 }, normal, Color.white);
        }
        //md.AddQuad(
        //  new Vector3[] {
        //    center + new Vector3(-s, h, -s),
        //    center + new Vector3(-s, h, +s),
        //    center + new Vector3(+s, h, +s),
        //    center + new Vector3(+s, h, -s)
        //  },
        //  Vector3.up, Color.white);
      }
    }
    md.CreateMesh(mesh);
  }
}
