﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorkpenManager : MonoBehaviour {

  GameObject group;

  public Workpen goldWorkpenPrefab;
  public Workpen cloneWorkpenPrefab;

  List<Workpen> workpens = new List<Workpen>();

  public IEnumerable<Workpen> Workpens { get { return workpens; } }

  void Awake() {
    group = gameObject.EmptyGroup("workpens");
  }

  public void CreateWorkpen(Vector2 mp, Workpen.Type type) {
    Vector2Int cell = MapBakery.PositionToCell(mp);
    var tile = Game.I.Map.At(cell);
    if (tile == null) {
      return;
    }
    if (tile.IsBlocked) {
      return;
    }
    Workpen prefab = type == Workpen.Type.CLONE ? cloneWorkpenPrefab : goldWorkpenPrefab;
    if (Game.I.TotalGold > prefab.price) {
      Workpen workpen = Instantiate(prefab);
      workpen.transform.parent = group.transform;
      workpen.SetCell(cell);
      workpens.Add(workpen);
      workpen.StartConstruction();
    }
  }

  //public void RemoveWorkpen(Vector2 mp) {
  //  Vector2Int cell = MapBakery.PositionToCell(mp);
  //  var tile = Game.I.Map.At(cell);
  //  if (tile == null) {
  //    return;
  //  }
  //  if (tile.workpen == null) {
  //    return;
  //  }
  //  Destroy(tile.workpen.gameObject);
  //  tile.workpen = null;
  //  workpens.Remove(workpen);
  //}
}
