﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlobManager : MonoBehaviour {

  GameObject group;

  public Blob blobPrefab;

  List<Blob> blobs = new List<Blob>();

  public IEnumerable<Blob> Blobs {  get { return blobs; } }

  void Awake() {
    group = gameObject.EmptyGroup("blobs");
  }

  void Start() {
    CreateRandom(Game.I.Map, RNG.Instance);
	}
	
	void Update() {
	}

  public int count = 25;

  void CreateRandom(Map map, RNG rng) {
    for (int i=0; i<count; i++) {
      Blob blob = GameObject.Instantiate(blobPrefab);
      blob.transform.parent = group.transform;
      Vector2 position = rng.RandomVector2(0f, map.Rows, 0f, map.Cols);
      blob.Reposition(Game.I.map, position);
      blobs.Add(blob);
    }
  }

  public void CloneBlob(Blob original) {
    Blob blob = GameObject.Instantiate(blobPrefab);
    blob.transform.parent = group.transform;
    blob.Reposition(Game.I.map, original.Position);
    blobs.Add(blob);
  }
}
