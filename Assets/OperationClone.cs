﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OperationClone : MonoBehaviour, Operation {

  public Workpen workpen;

  public float clonePrice = 1000f;

  float progress = 0f;

  public float Progress() {
    return Mathf.Min(progress / clonePrice, 1f);
  }

  public void WorkOnIt(float delta) {
    progress += delta;
    if (progress >= clonePrice) {
      progress -= clonePrice;
      if (workpen.worker != null) {
        Game.I.blobManager.CloneBlob(workpen.worker);
        if (audioSource != null) {
          audioSource.PlayOneShot(clips[RNG.Instance.RandomInt(clips.Length)]);
        }
      }
    }
  }

  AudioSource audioSource;

  public AudioClip[] clips;

  void Awake() {
    audioSource = GetComponent<AudioSource>();
    workpen.operation = this;
  }
}
