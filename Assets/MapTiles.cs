﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[RequireComponent(typeof(MeshFilter))]
public class MapTiles : MonoBehaviour {

  Mesh mesh;

  void Start() {
    mesh = new Mesh();
    GetComponent<MeshFilter>().mesh = mesh;
    InvokeRepeating("UpdateTiles", 0f, 0.25f);
  }

  void UpdateTiles() {
    Bake(Game.I.Map, mesh);
  }

  public enum ColorMode {
    None,
    Faction,
    BlobCount
  }

  public ColorMode colorMode = ColorMode.Faction;

  public Color defaultTileColor = Color.gray;

  Color GetTileColor(Tile tile) {
    switch (colorMode) {
      default: case ColorMode.None: return defaultTileColor;
      case ColorMode.Faction: return GetFactionColor(tile.faction);
      case ColorMode.BlobCount: {
        int n = tile.Blobs.Count();
        return Color.HSVToRGB(0.20f, (float)n / (float)3, 0.216f);
      }
      //case ColorMode.CurrentRole: return Game.I.parameters.workColors[tile.workpen != null ? (int)tile.workpen.TopRole : 0];
      //case ColorMode.AllowedRole: return Game.I.parameters.workColors[tile.workpen != null ? (int)tile.workpen.MaxAllowedRole : 0];
      //case ColorMode.SupportedRole: return Game.I.parameters.workColors[tile.workpen != null ? (int)tile.workpen.MaxSupportedRole : 0];
    }
  }

  public Color[] factionColors;

  Color GetFactionColor(int faction) {
    if (factionColors.Length == 0) {
      return Color.magenta;
    }
    if (faction < 0 || factionColors.Length <= faction) {
      faction = 0;
    }
    return factionColors[faction];
  }

  public float tileScale = 0.95f;

  /// <summary>
  /// Work Cells:
  /// Gold: 
  /// </summary>
  public void Bake(Map map, Mesh mesh) {
    MeshData md = new MeshData();
    float s = 0.5f * tileScale;
    for (int row = 0; row < map.Rows; row++) {
      for (int col = 0; col < map.Cols; col++) {
        Tile tile = map.At(row, col);
        Vector3 center = MapBakery.CellCenter3(row, col);
        md.AddQuad(
          new Vector3[] {
            center + new Vector3(-s, 0f, -s),
            center + new Vector3(-s, 0f, +s),
            center + new Vector3(+s, 0f, +s),
            center + new Vector3(+s, 0f, -s)
          },
          Vector3.up, GetTileColor(tile));
      }
    }
    md.CreateMesh(mesh);
  }
}
