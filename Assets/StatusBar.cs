﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class StatusBar : MonoBehaviour {

  public UnityEngine.UI.Text goldText;
  public UnityEngine.UI.Text workBlobText;
  public UnityEngine.UI.Text idleBlobText;

  void Start() {
	}
	
	void Update() {
    goldText.text = string.Format("Gold: {0}", Mathf.RoundToInt(Game.I.TotalGold));
    int work_blobs = Game.I.blobManager.Blobs.Count(b => !b.IsIdle);
    workBlobText.text = string.Format("Working Blobs: {0}", work_blobs);
    int idle_blobs = Game.I.blobManager.Blobs.Count(b => b.IsIdle);
    idleBlobText.text = string.Format("Idle Blobs: {0}", idle_blobs);
  }
}
