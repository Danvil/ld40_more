﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Workpen : MonoBehaviour {

  public float productionRate = 10f;
  public float price = 1000f;

  public GameObject solidObj;
  public GameObject transparentObj;
  public GameObject workDidoObj;

  Tile tile;

  public Vector2Int cell { get { return tile.cell; } }

  public Blob worker;
  public Dooda dooda;

  public Operation operation;

  public void SetCell(Vector2Int cell) {
    tile = Game.I.map.At(cell);
    tile.workpen = this;
    transform.position = MapBakery.CellAnchor3(cell);
  }

  public float ProductionPercentage {
    get { return operation.Progress(); }
  }

  float stillToPayForConstruction = 0f;
  public float StillToPayForConstruction {  get { return stillToPayForConstruction; } }

  public void StartConstruction() {
    stillToPayForConstruction = price;
  }

  public bool IsOperational { get { return stillToPayForConstruction == 0f; } }

  public float AddGoldForConstruction(float money) {
    money = Mathf.Min(money, stillToPayForConstruction);
    stillToPayForConstruction -= money;
    float construction_height = -0.25f * stillToPayForConstruction / price;
    solidObj.transform.position = new Vector3(solidObj.transform.position.x, construction_height, solidObj.transform.position.z);
    if (IsOperational) {
      transparentObj.SetActive(false);
    }
    return money;
  }

  public bool consumesGold = false;

  public enum Type {
    GOLD, CLONE
  }
  public Type type = Type.GOLD;

  public bool IsValidInput(Dooda dooda) {
    if (dooda == null) {
      return false;
    }
    if (type == Type.GOLD) {
      return dooda.resource == Resource.ROCK || dooda.resource == Resource.TREE || dooda.resource == Resource.GRASS;
    } else if (type == Type.CLONE) {
      return dooda.resource == Resource.GOLD;
    } else {
      return false;
    }
  }

  public System.Action<float> onWorkProgress = null;

  void Update() {
    if (IsOperational && worker != null && worker.CurrentCell == cell) {
      if (dooda != null && IsValidInput(dooda) && dooda.amount > 0f) {
        float dgold = productionRate * Time.deltaTime;
        dooda.amount -= dgold / dooda.conversionRate;
        operation.WorkOnIt(dgold);
        // update dido
        workDidoObj.transform.localRotation = Quaternion.Euler(0f, 360.0f * ProductionPercentage, 0f);
      }
      if (dooda != null && dooda.amount <= 0f) {
        GameObject.Destroy(dooda.gameObject);
        dooda = null;
      }
    }
  }
}
