﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class Tile {
  public Tile(Vector2Int cell) {
    this.cell = cell;
  }

  public Vector2Int cell { get; private set; }

  public Workpen workpen;
  public bool isWorkpen { get { return workpen != null; } }
  public int faction = 0;

  public Dooda dooda;

  public bool IsBlocked {
    get {
      return workpen != null || (dooda != null && dooda.resource != Resource.GOLD && dooda.resource != Resource.GRASS);
    }
  }

  #region gold

  float gold = 0;
  // Returns how much gold was added
  public float AddGold(float dgold) {
    if (dgold < 0) {
      throw new Exception("must not be negative");
    }
    dgold = Mathf.Min(gold + dgold, Game.I.maxGoldPerTile) - gold;
    gold += dgold;
    goldSpeedFactor = SpeedPerGold(gold);
    return dgold;
  }
  // Returns how much gold was removed
  public float RemoveGold(float dgold) {
    if (dgold < 0) {
      throw new Exception("must not be negative");
    }
    dgold = Mathf.Min(dgold, gold);
    gold -= dgold;
    goldSpeedFactor = SpeedPerGold(gold);
    return dgold;
  }
  public float Gold { get { return gold; } }

  #endregion

  #region blobs

  List<Blob> blobs = new List<Blob>();
  public void RemoveBlob(Blob blob) {
    blobs.Remove(blob);
    blobSpeedFactor = BlobSpeed(blobs.Count);
  }
  public void AddBlob(Blob blob) {
    blobs.Add(blob);
    blobSpeedFactor = BlobSpeed(blobs.Count);
  }
  public IEnumerable<Blob> Blobs { get { return blobs; } }

  #endregion

  #region role support from neighbouring workpens

  int[] roleCoverageCount = new int[(int)Role._count];
  public void AddRoleCoverage(Role role) {
    roleCoverageCount[(int)role]++;
  }
  public void RemoveRoleCoverage(Role role) {
    roleCoverageCount[(int)role]--;
  }
  public bool HasRoleCoverage(Role role) {
    return roleCoverageCount[(int)role] > 0;
  }

  #endregion

  #region speed

  public float doodaSpeedFactor = 0.75f;

  float goldSpeedFactor = 1f;
  float blobSpeedFactor = 1f;

  static float BlobSpeed(int count) {
    return Mathf.Max(0.25f, 1f / (0.25f + 0.75f * count));
  }

  static float SpeedPerGold(float gold) {
    return Mathf.Max(0.25f, 1f / (1f + 0.02f * gold));
  }

  public float Speed {
    get {
      float dooda_factor = dooda == null ? 1f : doodaSpeedFactor;
      return dooda_factor * blobSpeedFactor * goldSpeedFactor;
    }
  }

  #endregion
}
