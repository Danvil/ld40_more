﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface Operation {
  float Progress();
  void WorkOnIt(float delta);
}

public class OperationGold : MonoBehaviour, Operation {

  public float goldSubmitTreshold = 25f;
  public float goldExplosionRadius = 5f;

  public Workpen workpen;

  float gold = 0f;

  public float Progress() {
    return Mathf.Min(1f, gold / goldSubmitTreshold);
  }

  public void WorkOnIt(float dgold) {
    gold += dgold;
    if (gold >= goldSubmitTreshold) {
      gold -= Game.I.AddGold(workpen.cell, goldExplosionRadius, goldSubmitTreshold);
      if (audioSource != null) {
        audioSource.PlayOneShot(clips[RNG.Instance.RandomInt(clips.Length)]);
      }
    }
  }

  AudioSource audioSource;

  public AudioClip[] clips;

  void Awake() {
    audioSource = GetComponent<AudioSource>();
    workpen.operation = this;
  }
}
