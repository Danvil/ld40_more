﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System;

public static class GameObjectExtensions {
  // Creates an empty child game object with given name (deletes first if already exists)
  public static GameObject EmptyGroup(this GameObject go, string name) {
    Transform childTransform = go.transform.Find(name);
    if (childTransform != null) {
      GameObject.DestroyImmediate(childTransform.gameObject);
    }
    GameObject child = new GameObject();
    child.name = name;
    child.transform.parent = go.transform;
    child.transform.localPosition = Vector3.zero;
    return child;
  }

  public static List<Vector3> Clone(this List<Vector3> values) {
    return values.Select(x => x).ToList();
  }
  public static List<Color> Clone(this List<Color> values) {
    return values.Select(x => x).ToList();
  }
  public static List<int> Clone(this List<int> values) {
    return values.Select(x => x).ToList();
  }

  public static int Square(this int x) {
    return x * x;
  }

  public static Vector3 ToVector3(this Vector2 p) {
    return new Vector3(p.x, 0f, p.y);
  }

  public static T[] Shuffle<T>(this IEnumerable<T> array, RNG rng) {
    return array.OrderBy(x => rng.RandomInt(1000000)).ToArray();
  }
}
